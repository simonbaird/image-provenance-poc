# Image Provenace PoC

This repository illustrates how to generate a SLSA Provenance for a container image built via the
GitLab CI, and how to use [Enterprise Contract](https://enterprisecontract.dev/) to verify it.

NOTE: The process documented here relies on manual steps to sign and attest container images. A more
advanced scenario involves some sort of automation. This creates the opportunity to provide an
observer-like pattern to generate attestations signed by a centralized entity.

## Generate the SLSA Provenace Predicate

The first step in attesting a container image is to generate a predicate. For this example, we will
the built-in GitLab mechanism to create
[artifact provenance metadata](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-provenance-metadata).

The file [.gitlab-ci.yml](.gitlab-ci.yml) in this repo implements this by first enabling the feature
through the variables `RUNNER_GENERATE_ARTIFACTS_METADATA` and `SLSA_PROVENANCE_SCHEMA_VERSION`.
Then, the `image-manifest` job is responsible for creating an artifact to be attested.

It is important to note that the GiLab feature will only generate the provenance metadata for actual
artifacts produced by the pipeline. In most cases, a pipeline that builds a container image simply
pushes the image to a container registry instead of creating a downloadable version of the image as
an artifact. For this reason, the `image-manifest` job emits an artifact. It uses the Image Manifest
as the artifact because it shares the digest of the image reference on the registry. For all
purposes, the Image Manifest is the same entity as the image on the container registry.

Once the pipeline completes successfully, the artifact can be downloaded. This is a zip file that
contains the artifact as well as the provenance metadata. The provenance metadata is wrapped in an
in-toto statement. Here, we are only interested in the `.predicate` attribute. Use a tool like `jq`
to extract the data we care about:

```text
jq .predicate artifacts-metadata.json > predicate.json
```

## Sign and Attest the Image

Once we have the expected `predicate.json` file, it is time to move on towards signing and attesting
a container image.

This section assumes a long-lived cosign key for the sake of simplicity. The `cosign` commands below
can easily be adapted to better suit your needs.

First, sign the image:

```text
$ cosign sign --key=cosign.key $IMAGE
$ cosign attest --key=cosign.key --predicate predicate.json \
  --type https://slsa.dev/provenance/v1 $IMAGE
```

`$IMAGE` is a variable containing the container image reference.

`cosign tree` is useful to verify the image does contain a signature and an attestation.

## Verify the Image

Now we are ready to verify the image is properly signed and attested. For this task, we will use
the Enterprise Contract CLI tool.

```text
ec validate image --public-key cosign.pub --image $IMAGE --output yaml
```

If the command is successful, it means the image is signed and attested as expected. This is the
most basic test that can be performed with EC.
